package projet.si.prototype.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import projet.si.prototype.repo.NormeRepository;
import projet.si.prototype.repo.ObjetRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Api {
    private static List<Norme> normes = new ArrayList<>();
    private static List<Objet> objets = new ArrayList<>();

    @Autowired
    NormeRepository normeRepository;
    @Autowired
    ObjetRepository objetRepository;


    @GetMapping("/normes")
    public ResponseEntity<List<Norme>> getAllNormes() {
        try {
            return ResponseEntity.ok(normeRepository.findAll());
        } catch (Exception e) {
            return ResponseEntity.ok(new ArrayList<>());
        }
    }

    @GetMapping("/normes/{id}")
    public ResponseEntity<Norme> getNorme(@PathVariable String id) {
        return ResponseEntity.ok(normeRepository.findById(id).get());
    }

    @GetMapping("/objets")
    public ResponseEntity<List<Objet>> getAllObjets(@RequestParam String type) {
        return ResponseEntity.ok(objetRepository.findAllByType(type));
    }

    @GetMapping("/objets/{id}")
    public ResponseEntity<Objet> getObjet(@PathVariable String id) {
        return ResponseEntity.ok(objetRepository.findById(id).get());
    }

    @GetMapping("/risques")
    public ResponseEntity<String> getRisquesByObjet(@RequestParam String marque, @RequestParam String modele) {
        Objet objet = objetRepository.findTopByMarqueAndModele(marque, modele);
        //Risque maximum
        String maxCvss = "0";
        for (Faille f : objet.liste_failles) {
            if (Float.parseFloat(f.getCvss()) > Float.parseFloat(maxCvss)) {
                maxCvss = f.getCvss();
            }
        }
        return ResponseEntity.ok(maxCvss);
    }

}
