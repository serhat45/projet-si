package projet.si.prototype.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import projet.si.prototype.controller.Objet;

import java.util.List;

@Repository
public interface ObjetRepository extends MongoRepository<Objet, String> {

    List<Objet> findAllByType(String type);

    Objet findTopByMarqueAndModele(String marque, String modele);
}
