FROM openjdk:11
VOLUME /tmp
ADD livrable/webservice.jar webservice.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","webservice.jar"]
