conn = new Mongo();
db = conn.getDB("risques");

db.createCollection("risque");
db.createCollection("norme");
db.createCollection("objet");

db.norme.insert( { nom: "CC_EAL6", description: "Certification critère commun EAL6", valeur: 6 } );
db.norme.insert( { nom: "ISO_27001", description: "Norme ISO 27001 MANAGEMENT DE LA SÉCURITÉ DE L'INFORMATION", valeur: 3 } );
db.norme.insert( { nom: "SRA_S7", description: "Norme SRA niveau S7", valeur: 3 } );
db.norme.insert( { nom: "CC_EAL5", description: "Certification critère commun EAL5", valeur: 5 } );
db.norme.insert( { nom: "CC_EAL4", description: "Certification critère commun EAL4", valeur: 4 } );
db.norme.insert( { nom: "CC_EAL3", description: "Certification critère commun EAL3", valeur: 3 } );
db.norme.insert( { nom: "CC_EAL2", description: "Certification critère commun EAL2", valeur: 2 } );
db.norme.insert( { nom: "CSPN", description: "Certification de sécurité de premier niveau des produits des technologies de l’information", valeur: 1 } );

db.objet.insert( { modele: "galaxy_a7", marque: "samsung", type: "Telephone", description: "Samsung Galaxy A7 smartphone", liste_certifications: ["ISO_27001","CC_EAL6"], liste_failles: [{cve: "CVE-2019-15435", cvss: "4.3"}], mot_clef: ["telephone","téléphone","mobile","samsung","Android 8.0 Oreo","Android 10"]} );
db.objet.insert( { modele: "model_3", marque: "tesla", type: "Voiture", description: " Tesla Model S voiture autonome", liste_certifications: ["CC_EAL5","SRA_S7"], liste_failles: [{cve: "CVE-2020-15912", cvss: "3.3"},{cve: "CVE-2019-9977", cvss: "6.8"}], mot_clef: ["voiture","autonome","tesla","Ubuntu"]} );
db.objet.insert( { modele: "Apple Watch 7", marque: "Apple", type: "Montre", description: "Apple watch 7 montre connectée", liste_certifications: ["ISO_27001","CC_EAL6"], liste_failles: [{cve: "CVE-2015-5918", cvss: "7.2"},{cve: "CVE-2015-5919", cvss: "7.2"}], mot_clef: ["montre","apple","WatchOS"]} );
db.objet.insert( { modele: "DS3-CrossBack", marque: "DS Automobiles", type: "Voiture", description: "DS3 Crossback voiture connectée", liste_certifications: ["CC_EAL2","SRA_S7"], liste_failles: [{cve: "CVE-2019-5918", cvss: "9.2"},{cve: "CVE-2019-7314", cvss: "6.5"}], mot_clef: ["voiture","ds"]} );
db.objet.insert( { modele: "Passat", marque: "Volkswagen", type: "Voiture", description: "Volkswagen Passat voiture connectée", liste_certifications: ["CC_EAL4","SRA_S7"], liste_failles: [{cve: "CVE-2020-2045", cvss: "8.3"}], mot_clef: ["voiture","volkswagen"]} );
db.objet.insert( { modele: "V60", marque: "Volvo", type: "Voiture", description: "Volvo V60 voiture connectée", liste_certifications: ["CC_EAL3"], liste_failles: [{cve: "CVE-2018-6734", cvss: "2.7"}], mot_clef: ["voiture","volvo"]} );
