# Projet-SI

## Contexte
***

- Augmentation nombre d’objets connectés 
- Sécurisation variable de ces objets
- Jurisprudence qui pousse à indemniser les conséquences de piratages
- Reconnaissances officielle de l’utilisation frauduleuse des objets
- Apparition de Nouvelle garantie dans les contrats

## Concept
***

Proposer une Application pour évaluer les risques de piratages liées aux objets connectés et pour pouvoir répercuter ces risques dans les contrats d’assurance de manière fiable.

## Plus-value
***

Sur les contrats :
- meilleure évaluation de la sinistralité des objets connectés
- évolution des tarifs en conséquence

Dans la prévention des risques :
- communication de conseils adapté pour limiter les sinistres
- alerte en cas de détection de faille de sécurité
axe fraude

# Le prototype

## Pré-requis 
***

- docker 
- un IDE type IntelliJ (optionnel)
- java (optionnel)
- curl (optionnel)

## Lancer le projet
***

Dans le dossier racine du projet "projet-si" exécuter les commandes suivantes :
```ignorelang
docker-compose up -d
```

Pour vérifier que les services sont bien lancé exécuter la commande suivante :
```ignorelang
docker-compose ps
```

Vous devez voir 2 services tourner, le premier "projet-si-mongodb" sur le port 21017
et "webservice" sur le port 8080.

Si le web service n'est pas "Up" vous pouvez ouvrir le dossier "prototype" avec intelliJ 
pour lancer en local.

## Appeler le service
***

Avec un navigateur vous pouvez attaquer les URIS suivantes :
- http://localhost:8080/normes --> retournes toutes les normes de la base de données
- http://localhost:8080/normes/{id} --> retourne une norme en particulier
- http://localhost:8080/objets?type=Voiture --> retourne tous les objets du type demandé
- http://localhost:8080/objets/{id} --> retourne un objet en particulier
- http://localhost:8080/risques?modele=model_3&marque=tesla --> retourne le risque maximum pour l'objet

Vous pouvez attaquer les même URI avec curl dans un terminal.

Ou Utiliser le fichier "generated-request.http" dans le dossier "prototype/src/main/ressources" avec
intelliJ.

## Regarder la base de donnée

Vous pouvez consulter le fichier "ini.js" pour voir les données inséré en base de données.
Pour avoir les idenfiants (auto-généré) vous pouvez consulter la base de données mongo en axécutant les commandes 
suivante :

```ignorelang
docker exec -it projet-si-mongodb bash
mongo
use risques;
db.norme.find();
db.objet.find();
```
