package projet.si.prototype.controller;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class Objet {
    @Id
    public String id;
    public String modele;
    public String marque;
    public String description;
    public String type;
    public List<String> liste_certifications;
    public List<Faille> liste_failles;
    public String mot_clef;

    public Objet() {
    }


    public Objet(String modele, String marque, String description, String type, List<String> liste_certifications, List<Faille> liste_failles, String mot_clef) {
        this.modele = modele;
        this.marque = marque;
        this.description = description;
        this.type = type;
        this.liste_certifications = liste_certifications;
        this.liste_failles = liste_failles;
        this.mot_clef = mot_clef;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getCertifications() {
        return liste_certifications;
    }

    public void setCertifications(List<String> liste_certifications) {
        this.liste_certifications = liste_certifications;
    }

    public List<Faille> getFailles() {
        return liste_failles;
    }

    public void setFailles(List<Faille> liste_failles) {
        this.liste_failles = liste_failles;
    }

    public String getMotsCles() {
        return mot_clef;
    }

    public void setMotsCles(String mot_clef) {
        this.mot_clef = mot_clef;
    }

    @Override
    public String toString() {
        return "Objet{" +
                "id=" + id +
                ", modele='" + modele + '\'' +
                ", marque='" + marque + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", liste_certifications=" + liste_certifications +
                ", liste_failles=" + liste_failles +
                ", mot_clef='" + mot_clef + '\'' +
                '}';
    }
}
